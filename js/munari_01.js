/**
 * Physical playground with simulated forces.
 * 
 */

/////////////////////////// GLOBALS ////////////////////////////
let micro;
let amplitude;
let font;
let sensitivity = 50;
let cnv;

// Ici, il s'agit de 3 objets qui sont créées grâce à notre classe Texture
let tx1, tx2, tx3;

let seuilSonore = 2; // seuil à partir duquel on applique un mouvement avec le micro
let timer = 0;
let timerMax = 70; // valeur qui détermine la durée du mouvement appliqué grâce au son
let isForce = false;
let img1, img2, img3; // les images
/////////////////////////// SETUP ////////////////////////////
function preload() {
    img1 = loadImage("media/test2.jpg"); // image #1
    img1.resize(600, 0); //taille de l'image
    img2 = loadImage("media/test11.jpg"); // image #2
    img2.resize(600, 0); //taille de l'image
    img3 = loadImage("media/test1.jpg"); // image #3
    img3.resize(600, 0); //taille de l'image
}

function setup() {
    
let cnv = createCanvas(windowWidth, windowHeight, WEBGL);
//var x = (windowWidth - width) / 2;
//var y = (windowHeight - height) / 2;
//cnv.position(x, y);


    //cnv = createCanvas(1080, 720, WEBGL);
    cnv.parent('theCanvas');
    //font = loadFont('font/neue.otf');
textSize(20);
    background(0);
    smooth();
    frameRate(25);

    // on crée chaque objet avec une image associé
    tx1 = new Texture(img1);
    tx2 = new Texture(img2);
    tx3 = new Texture(img3);
    micro = new p5.AudioIn();
    micro.start();
}

/////////////////////////// DRAW ////////////////////////////
function draw() {
    background(0);
    //text('Bruno Munari', 12, 30);

    let soufle = createVector(); // vecteur qui gère le mouvement lorsqu'on souffle  
    //float micro = in.mix.level() * intensity; 
    let level = micro.getLevel() * sensitivity; // get micro input 

    // voici les méthodes/fonctions associées à nos objets et qui gèrent
    // des comportements différents. Note bien la deuxième valeur dans la
    // méthode drawZone car elle gère le mouvement de chaque objet. 
    // Jouer avec ces valeurs !! petites valeurs créent beaucoup de changements !!

    tx1.drawZone_01(2.5, 3.8); // afficher la zone avec une valeur d'échelle et une valeur pour la vélocité
    tx2.drawZone_02(1.45, 4.3);
    tx3.drawZone_03(0.75, 5);

    // On vérifie le niveau sonore/le seuil à partir duquel
    // on change l'état de notre boolean isForce
    if (level > seuilSonore && timer == 0) {
        isForce = true;
    }

    // si isForce est vrai, on applique une force en fonction des valeurs d'entrée du son
    if (isForce) {

        soufle = createVector(random(-level, level), random(-level, level));
        // la deuxième valeur gère le niveau de la force. Jouer avec ces valeurs
        tx1.applyForce(soufle, 3.5);
        tx2.applyForce(soufle, 3);
        tx3.applyForce(soufle, 1.5);

        // on utilse un timer pour appliquer la force pendant un certain temps
        timer++;
        if (timer > timerMax) {
            timer = 0;
            isForce = false;
        }
    }
    // s'il n'y a pas de son, on applique une force d'attraction vers le centre de nos éléments
    // la valeur entre parenthèse gère ce niveau d'attraction
    else {
        tx1.attract(0.19);
        tx2.attract(0.24);
        tx3.attract(0.2);
    }
    //println(timer);
}


/////////////////////////////////////////////////////////>>>>>> LA CLASSE
/**
 * Une classe qui englobe des données et des méthodes
 * Tu trouveras notamment les fonctions de dessin 
 * pour chaque texture
 */


class Texture {
    constructor(img) {
        this.loc = new createVector(-width/2, -height/2, 0);
        this.vel = new createVector(random(-10, 10), random(-10, 10), 0);
        //vel = new PVector(random(-1,1), random(-1,1));
        this.accel = 1.73;
        this.acc = new createVector(random(-this.accel, this.accel), random(-this.accel, this.accel));
        this.theImage = img;
        this.forme = createGraphics(img1.width, img1.height);
        this.rond = createGraphics(img2.width, img2.height);
        this.demi = createGraphics(img3.width, img3.height);
    }


    ////////////////////////////////////////////////// >>>>
    // presque les mêmes fonctions que les originaux
    zone_01(scaling, tx) {
        push();
        translate(this.loc.x, this.loc.y);
        scale(scaling);
        //float randAngle = random(360); //rotation aléatoire
        //rotate(radians(randAngle)); 
        //beginShape();
        //fill('rgba(255, 0, 0, 1)');
        noStroke();
        rotateZ(frameCount * 0.01);
        rotateX(frameCount * 0.01);
        rotateY(frameCount * 0.01);
        //textureWrap(CLAMP);
        texture(tx);

        box(80);
        /*
        vertex(0, 0, 0, 0);
        vertex(200, 0, 200, 0);
        vertex(150, 200, 150, 200);
        vertex(200, 400, 200, 400);
        vertex(0, 400, 0, 400);
        vertex(50, 200, 50, 200);
        */
        //endShape();
    
        pop();
    }

    zone_02(scaling, tx) {
        push();
        translate(this.loc.x, this.loc.y);
        scale(scaling);
        this.rond.fill('rgba(0, 0, 0, 1)');
        this.rond.circle(tx.width / 2, tx.height / 2, tx.width);

        tx.mask(this.rond);
        image(tx, 0, 0);
        pop();
    }

    ////////////////////////////////////////////////

    zone_03(scaling, tx) {
        push();
        translate(this.loc.x, this.loc.y);
        scale(scaling);
        this.demi.fill('rgba(0, 0, 0, 1)');
        this.demi.arc(tx.width / 2, tx.height / 2, tx.width, tx.height, 0, PI);

        tx.mask(this.demi);
        image(tx, 0, 0);
        pop();
    }

    ////////////////////////////////////////////////// >>>> afficher les textures
    drawZone_01(scaling, v) {
        this.zone_01(scaling, this.theImage);
        this.update(v);
    }
    drawZone_02(scaling, v) {
        this.zone_02(scaling, this.theImage);
        this.update(v);
    }
    drawZone_03(scaling, v) {
        this.zone_03(scaling, this.theImage);
        this.update(v);
    }

    ////////////////////////////////////////////////// >>>> Rien à faire ici
    update(velLimit) {
        this.vel.add(this.acc);
        this.loc.add(this.vel);
        this.vel.limit(velLimit); // Limit our acceleration to 5.
        this.acc.mult(0);
    }

    attract(attractionForce) {
        let centerPos = createVector(-width/10, -height/10, 0);
        let dir = p5.Vector.sub(centerPos, this.loc);
        dir.normalize();
        dir.mult(attractionForce); //Scale up our vector
        this.acc = dir; //Assign the value of dir to acc.
    }


    applyForce(force, s) {
        force.mult(s);
        //PVector r = PVector.random2D();
        //r.mult(3.25);
        //acc.add(r);
        this.acc.add(force);
    }

    check() {
        if (this.loc.x > width) {
            this.loc.x = 0;
        }
        if (this.loc.x < 0) {
            this.loc.x = width;
        }

        if (this.loc.y > height) {
            this.loc.y = 0;
        }
        if (this.loc.y < 0) {
            this.loc.y = height;
        }
    }
}