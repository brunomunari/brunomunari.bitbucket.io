var colors = [4];
var seed;  
var cellSize = 42;


function setup() {
  createCanvas(586, 586);
  fill(255);
  noStroke();

  colors[0] = color(215, 57, 128);
  colors[1] = color(0, 143, 0);
  colors[2] = color(27, 58, 144);
  colors[3] = color(0);
  seed = random(5000);
  
}

function draw() {
  background(255); 
  seed = random(5000);

  for (var x = 40; x<600; x+=(cellSize*2))
  for (var y = 40; y<600; y+=(cellSize*2)){
  module(x, y, cellSize);

  }
}

function module(x, y, s) {
  rectMode(CENTER);

  push();
 translate(x, y);
  rotate(radians(0));
  fill((colors[int(random(colors.length))]));
  rect(-s/2, -s/2, s, s);
  fill((colors[int(random(colors.length))]));
  rect(s/2, -s/2, s, s);
  fill((colors[int(random(colors.length))]));
  rect(s/2, s/2, s, s);
  fill((colors[int(random(colors.length))]));
  rect(-s/2, s/2, s, s);
  fill((colors[int(random(colors.length))]));
  pop();

   push();
  translate(x, y);
  rotate(radians(45));
  fill((colors[int(random(colors.length))]));
  rect(0, 0, s/1.5, s/1.5);
  pop();
}


function keyPressed() {
  randomSeed(5000);
}